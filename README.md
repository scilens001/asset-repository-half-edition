# Island Adventures Asset Repository

This is a collection of custom assets for the [Island Adventures](https://gitlab.com/IslandAdventures/IslandAdventures) mod.

You only need the main mod to play. Ignore this repository unless you're a developer.
